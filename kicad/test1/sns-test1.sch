EESchema Schematic File Version 2
LIBS:sns-kicad
LIBS:sns-test1-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Constant Const1
U 1 1 56FBE0F8
P 3800 2350
F 0 "Const1" H 3856 2743 60  0000 C CNN
F 1 "upper-limit" H 3856 2637 60  0000 C CNN
F 2 "" H 3600 2100 60  0000 C CNN
F 3 "" H 3600 2100 60  0000 C CNN
F 4 "1.0" H 3856 2531 60  0000 C CNN "init0"
F 5 "double" H 3800 2350 60  0001 C CNN "type"
	1    3800 2350
	1    0    0    -1  
$EndComp
$Comp
L Constant Const2
U 1 1 56FBE1A1
P 3800 3100
F 0 "Const2" H 3672 3206 60  0000 R CNN
F 1 "lower-limit" H 3672 3100 60  0000 R CNN
F 2 "" H 3600 2850 60  0000 C CNN
F 3 "" H 3600 2850 60  0000 C CNN
F 4 "0.9" H 3672 2994 60  0000 R CNN "init0"
F 5 "double" H 3800 3100 60  0001 C CNN "type"
	1    3800 3100
	1    0    0    -1  
$EndComp
$Comp
L Schmitt-Trigger ST1
U 1 1 56FBE831
P 5100 2700
F 0 "ST1" H 5100 3037 60  0000 C CNN
F 1 "upperWatcher" H 5100 2931 60  0000 C CNN
F 2 "" H 5000 2550 60  0000 C CNN
F 3 "" H 5000 2550 60  0000 C CNN
F 4 "double" H 4950 2400 60  0000 C CNN "type"
	1    5100 2700
	1    0    0    -1  
$EndComp
$Comp
L Input In1
U 1 1 56FBE9B1
P 2450 2700
F 0 "In1" H 2450 2950 60  0000 C CNN
F 1 "adc0" H 2450 2850 60  0000 C CNN
F 2 "" H 2250 2450 60  0000 C CNN
F 3 "" H 2250 2450 60  0000 C CNN
F 4 "double" H 2450 2550 60  0000 C CNN "type"
	1    2450 2700
	1    0    0    -1  
$EndComp
$Comp
L OutputPin OutPin1
U 1 1 56FBEDEE
P 6950 2650
F 0 "OutPin1" H 7028 2703 60  0000 L CNN
F 1 "tooHigh" H 7028 2597 60  0000 L CNN
F 2 "" H 6750 2400 60  0000 C CNN
F 3 "" H 6750 2400 60  0000 C CNN
F 4 "bool" H 6950 2650 60  0001 C CNN "type"
	1    6950 2650
	1    0    0    -1  
$EndComp
$Comp
L OutputPin OutPin3
U 1 1 56FBEE93
P 6950 4200
F 0 "OutPin3" H 7028 4253 60  0000 L CNN
F 1 "tooLow" H 7028 4147 60  0000 L CNN
F 2 "" H 6750 3950 60  0000 C CNN
F 3 "" H 6750 3950 60  0000 C CNN
F 4 "bool" H 6950 4200 60  0001 C CNN "type"
	1    6950 4200
	1    0    0    -1  
$EndComp
$Comp
L Constant Const3
U 1 1 56FBF2B3
P 3800 3800
F 0 "Const3" H 3856 4193 60  0000 C CNN
F 1 "upper-limit" H 3856 4087 60  0000 C CNN
F 2 "" H 3600 3550 60  0000 C CNN
F 3 "" H 3600 3550 60  0000 C CNN
F 4 "0.1" H 3856 3981 60  0000 C CNN "init0"
F 5 "double" H 3800 3800 60  0001 C CNN "type"
	1    3800 3800
	1    0    0    -1  
$EndComp
$Comp
L Constant Const4
U 1 1 56FBF2BA
P 3800 4600
F 0 "Const4" H 3672 4706 60  0000 R CNN
F 1 "lower-limit" H 3672 4600 60  0000 R CNN
F 2 "" H 3600 4350 60  0000 C CNN
F 3 "" H 3600 4350 60  0000 C CNN
F 4 "0.0" H 3672 4494 60  0000 R CNN "init0"
F 5 "double" H 3800 4600 60  0001 C CNN "type"
	1    3800 4600
	1    0    0    -1  
$EndComp
$Comp
L Schmitt-Trigger ST2
U 1 1 56FBF2C0
P 5100 4150
F 0 "ST2" H 5100 4487 60  0000 C CNN
F 1 "lowerWatcher" H 5100 4381 60  0000 C CNN
F 2 "" H 5000 4000 60  0000 C CNN
F 3 "" H 5000 4000 60  0000 C CNN
F 4 "double" H 4950 3850 60  0000 C CNN "type"
	1    5100 4150
	1    0    0    -1  
$EndComp
Wire Wire Line
	2650 2700 4700 2700
Wire Wire Line
	2900 4150 4700 4150
Wire Wire Line
	2900 4150 2900 2700
Connection ~ 2900 2700
Wire Wire Line
	6700 2650 5500 2650
Wire Wire Line
	6700 4200 5500 4200
$Comp
L And2 And1
U 1 1 56FBF535
P 6100 3450
F 0 "And1" H 6050 3737 60  0000 C CNN
F 1 "bothInRange" H 6050 3631 60  0000 C CNN
F 2 "" H 6000 3250 60  0000 C CNN
F 3 "" H 6000 3250 60  0000 C CNN
	1    6100 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	5600 2750 5500 2750
$Comp
L OutputPin OutPin2
U 1 1 56FBF5C8
P 6950 3450
F 0 "OutPin2" H 7027 3503 60  0000 L CNN
F 1 "withinRange" H 7027 3397 60  0000 L CNN
F 2 "" H 6750 3200 60  0000 C CNN
F 3 "" H 6750 3200 60  0000 C CNN
F 4 "bool" H 6950 3450 60  0001 C CNN "type"
	1    6950 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	5500 4100 5600 4100
Text Notes 4000 1650 0    100  ~ 20
Simple Limit Indicator
Wire Wire Line
	6700 3450 6400 3450
Wire Wire Line
	4700 2800 4400 2800
Wire Wire Line
	4400 2800 4400 3100
Wire Wire Line
	4400 3100 4000 3100
Wire Wire Line
	4700 4250 4400 4250
Wire Wire Line
	4400 4250 4400 4600
Wire Wire Line
	4400 4600 4000 4600
Text Label 4500 4350 0    60   ~ 0
clk1
Wire Wire Line
	4500 4350 4700 4350
Text Label 4500 2900 0    60   ~ 0
clk1
Wire Wire Line
	4500 2900 4700 2900
Wire Wire Line
	5600 4100 5600 3500
Wire Wire Line
	5600 3500 5700 3500
Wire Wire Line
	5700 3600 5350 3600
Text Label 5350 3600 0    60   ~ 0
clk1
Text Label 6500 2750 0    60   ~ 0
clk1
Wire Wire Line
	6500 2750 6700 2750
Text Label 6500 3550 0    60   ~ 0
clk1
Wire Wire Line
	6500 3550 6700 3550
Text Label 6500 4300 0    60   ~ 0
clk1
Wire Wire Line
	6500 4300 6700 4300
$Comp
L ClockMaster Clock1
U 1 1 56FC0FA5
P 2500 4450
F 0 "Clock1" H 2556 4737 60  0000 C CNN
F 1 "mainClock" H 2556 4631 60  0000 C CNN
F 2 "" H 2300 4200 60  0000 C CNN
F 3 "" H 2300 4200 60  0000 C CNN
F 4 "bool" H 2200 4200 60  0000 C CNN "type"
	1    2500 4450
	1    0    0    -1  
$EndComp
Text Label 3000 4450 0    60   ~ 0
clk1
Wire Wire Line
	3000 4450 2850 4450
Wire Wire Line
	5600 2750 5600 3400
Wire Wire Line
	5600 3400 5700 3400
Wire Wire Line
	4700 4050 4400 4050
Wire Wire Line
	4400 4050 4400 3800
Wire Wire Line
	4400 3800 4000 3800
Wire Wire Line
	4700 2600 4400 2600
Wire Wire Line
	4400 2600 4400 2350
Wire Wire Line
	4400 2350 4000 2350
$EndSCHEMATC
