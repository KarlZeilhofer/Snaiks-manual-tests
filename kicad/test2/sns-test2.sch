EESchema Schematic File Version 2
LIBS:sns-kicad
LIBS:sns-test2-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "SnS-Test 2"
Date "2016-04-07"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Constant Const1
U 1 1 56FBE0F8
P 4800 3100
F 0 "Const1" H 4856 3493 60  0000 C CNN
F 1 "upper-limit" H 4856 3387 60  0000 C CNN
F 2 "" H 4600 2850 60  0000 C CNN
F 3 "" H 4600 2850 60  0000 C CNN
F 4 "1.0" H 4856 3281 60  0000 C CNN "init0"
F 5 "double" H 4800 3100 60  0001 C CNN "type"
	1    4800 3100
	1    0    0    -1  
$EndComp
$Comp
L Constant Const2
U 1 1 56FBE1A1
P 4800 3850
F 0 "Const2" H 4672 3956 60  0000 R CNN
F 1 "lower-limit" H 4672 3850 60  0000 R CNN
F 2 "" H 4600 3600 60  0000 C CNN
F 3 "" H 4600 3600 60  0000 C CNN
F 4 "0.9" H 4672 3744 60  0000 R CNN "init0"
F 5 "double" H 4800 3850 60  0001 C CNN "type"
	1    4800 3850
	1    0    0    -1  
$EndComp
$Comp
L Schmitt-Trigger ST1
U 1 1 56FBE831
P 6100 3450
F 0 "ST1" H 6100 3909 60  0000 C CNN
F 1 "upperWatcher" H 6100 3803 60  0000 C CNN
F 2 "" H 6000 3300 60  0000 C CNN
F 3 "" H 6000 3300 60  0000 C CNN
F 4 "double" H 6100 3697 60  0000 C CNN "type"
	1    6100 3450
	1    0    0    -1  
$EndComp
$Comp
L Input In1
U 1 1 56FBE9B1
P 3450 3450
F 0 "In1" H 3506 3843 60  0000 C CNN
F 1 "adc0" H 3506 3737 60  0000 C CNN
F 2 "" H 3250 3200 60  0000 C CNN
F 3 "" H 3250 3200 60  0000 C CNN
F 4 "double" H 3506 3631 60  0000 C CNN "type"
	1    3450 3450
	1    0    0    -1  
$EndComp
$Comp
L OutputPin OutPin1
U 1 1 56FBEDEE
P 7950 3400
F 0 "OutPin1" H 8028 3464 60  0000 L CNN
F 1 "tooHigh" H 8028 3358 60  0000 L CNN
F 2 "" H 7750 3150 60  0000 C CNN
F 3 "" H 7750 3150 60  0000 C CNN
F 4 "bool" H 8028 3252 60  0000 L CNN "type"
	1    7950 3400
	1    0    0    -1  
$EndComp
$Comp
L OutputPin OutPin3
U 1 1 56FBEE93
P 7950 4950
F 0 "OutPin3" H 8028 5014 60  0000 L CNN
F 1 "tooLow" H 8028 4908 60  0000 L CNN
F 2 "" H 7750 4700 60  0000 C CNN
F 3 "" H 7750 4700 60  0000 C CNN
F 4 "bool" H 8028 4802 60  0000 L CNN "type"
	1    7950 4950
	1    0    0    -1  
$EndComp
$Comp
L Constant Const3
U 1 1 56FBF2B3
P 4800 4550
F 0 "Const3" H 4856 4943 60  0000 C CNN
F 1 "upper-limit" H 4856 4837 60  0000 C CNN
F 2 "" H 4600 4300 60  0000 C CNN
F 3 "" H 4600 4300 60  0000 C CNN
F 4 "0.1" H 4856 4731 60  0000 C CNN "init0"
F 5 "double" H 4800 4550 60  0001 C CNN "type"
	1    4800 4550
	1    0    0    -1  
$EndComp
$Comp
L Constant Const4
U 1 1 56FBF2BA
P 4800 5350
F 0 "Const4" H 4856 5169 60  0000 C CNN
F 1 "lower-limit" H 4856 5063 60  0000 C CNN
F 2 "" H 4600 5100 60  0000 C CNN
F 3 "" H 4600 5100 60  0000 C CNN
F 4 "0.0" H 4856 4957 60  0000 C CNN "init0"
F 5 "double" H 4800 5350 60  0001 C CNN "type"
	1    4800 5350
	1    0    0    -1  
$EndComp
$Comp
L Schmitt-Trigger ST2
U 1 1 56FBF2C0
P 6100 4900
F 0 "ST2" H 6100 5359 60  0000 C CNN
F 1 "lowerWatcher" H 6100 5253 60  0000 C CNN
F 2 "" H 6000 4750 60  0000 C CNN
F 3 "" H 6000 4750 60  0000 C CNN
F 4 "double" H 6100 5147 60  0000 C CNN "type"
	1    6100 4900
	1    0    0    -1  
$EndComp
Wire Wire Line
	3650 3450 5700 3450
Wire Wire Line
	3900 4900 5700 4900
Wire Wire Line
	3900 4900 3900 3450
Connection ~ 3900 3450
$Comp
L And2 And1
U 1 1 56FBF535
P 7100 4200
F 0 "And1" H 7050 4609 60  0000 C CNN
F 1 "bothInRange" H 7050 4503 60  0000 C CNN
F 2 "" H 7000 4000 60  0000 C CNN
F 3 "" H 7000 4000 60  0000 C CNN
F 4 "bool" H 7050 4397 60  0000 C CNN "type"
	1    7100 4200
	1    0    0    -1  
$EndComp
Wire Wire Line
	6600 3500 6500 3500
$Comp
L OutputPin OutPin2
U 1 1 56FBF5C8
P 7950 4200
F 0 "OutPin2" H 8027 4264 60  0000 L CNN
F 1 "withinRange" H 8027 4158 60  0000 L CNN
F 2 "" H 7750 3950 60  0000 C CNN
F 3 "" H 7750 3950 60  0000 C CNN
F 4 "bool" H 8027 4052 60  0000 L CNN "type"
	1    7950 4200
	1    0    0    -1  
$EndComp
Wire Wire Line
	6500 4850 6600 4850
Text Notes 2700 2600 0    100  ~ 20
test2: Simple Limit Indicator\nwith anti-glitch delays
Wire Wire Line
	7700 4200 7400 4200
Wire Wire Line
	5700 3550 5400 3550
Wire Wire Line
	5400 3550 5400 3850
Wire Wire Line
	5400 3850 5000 3850
Wire Wire Line
	5700 5000 5400 5000
Wire Wire Line
	5400 5000 5400 5350
Wire Wire Line
	5400 5350 5000 5350
Text Label 5500 5100 0    60   ~ 0
clk1
Wire Wire Line
	5500 5100 5700 5100
Text Label 5500 3650 0    60   ~ 0
clk1
Wire Wire Line
	5500 3650 5700 3650
Wire Wire Line
	6600 4850 6600 4250
Wire Wire Line
	6600 4250 6700 4250
Wire Wire Line
	6700 4350 6350 4350
Text Label 6350 4350 0    60   ~ 0
clk1
Text Label 7500 3500 0    60   ~ 0
clk1
Wire Wire Line
	7500 3500 7700 3500
Text Label 7500 4300 0    60   ~ 0
clk1
Wire Wire Line
	7500 4300 7700 4300
Text Label 7500 5050 0    60   ~ 0
clk1
Wire Wire Line
	7500 5050 7700 5050
$Comp
L ClockMaster Clock1
U 1 1 56FC0FA5
P 3500 5200
F 0 "Clock1" H 3475 5593 60  0000 C CNN
F 1 "mainClock" H 3475 5487 60  0000 C CNN
F 2 "" H 3300 4950 60  0000 C CNN
F 3 "" H 3300 4950 60  0000 C CNN
F 4 "bool" H 3475 5381 60  0000 C CNN "type"
	1    3500 5200
	1    0    0    -1  
$EndComp
Text Label 4000 5200 0    60   ~ 0
clk1
Wire Wire Line
	4000 5200 3850 5200
Wire Wire Line
	6600 3500 6600 4150
Wire Wire Line
	6600 4150 6700 4150
Wire Wire Line
	5700 4800 5400 4800
Wire Wire Line
	5400 4800 5400 4550
Wire Wire Line
	5400 4550 5000 4550
Wire Wire Line
	5700 3350 5400 3350
Wire Wire Line
	5400 3350 5400 3100
Wire Wire Line
	5400 3100 5000 3100
$Comp
L Delay Delay1
U 1 1 57069F78
P 7150 3400
F 0 "Delay1" H 7175 3759 60  0000 C CNN
F 1 "anti-glitch" H 7175 3653 60  0000 C CNN
F 2 "" H 6950 3150 60  0000 C CNN
F 3 "" H 6950 3150 60  0000 C CNN
F 4 "bool" H 7175 3547 60  0000 C CNN "type"
	1    7150 3400
	1    0    0    -1  
$EndComp
Wire Wire Line
	7450 3400 7700 3400
Wire Wire Line
	6900 3400 6500 3400
Text Label 6900 3700 0    60   ~ 0
clk1
Wire Wire Line
	6900 3700 6900 3500
$Comp
L Delay Delay2
U 1 1 5706A102
P 7150 4950
F 0 "Delay2" H 7175 5309 60  0000 C CNN
F 1 "anti-glitch" H 7175 5203 60  0000 C CNN
F 2 "" H 6950 4700 60  0000 C CNN
F 3 "" H 6950 4700 60  0000 C CNN
F 4 "bool" H 7175 5097 60  0000 C CNN "type"
	1    7150 4950
	1    0    0    -1  
$EndComp
Wire Wire Line
	7450 4950 7700 4950
Wire Wire Line
	6900 4950 6500 4950
Text Label 6900 5250 0    60   ~ 0
clk1
Wire Wire Line
	6900 5250 6900 5050
$EndSCHEMATC
