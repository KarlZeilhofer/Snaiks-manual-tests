TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    ../../libs/signals-and-systems/src/snsclockdevider.cpp \
    ../../libs/signals-and-systems/src/snsclockmaster.cpp \
    ../../libs/signals-and-systems/src/snsconstant.cpp \
    ../../libs/signals-and-systems/src/snsconverter.cpp \
    ../../libs/signals-and-systems/src/snshybrid.cpp \
    ../../libs/signals-and-systems/src/snsinput.cpp \
    ../../libs/signals-and-systems/src/snslogicand.cpp \
    ../../libs/signals-and-systems/src/snsmemorymanager.cpp \
    ../../libs/signals-and-systems/src/snsnumeric.cpp \
    ../../libs/signals-and-systems/src/snsoutputcout.cpp \
    ../../libs/signals-and-systems/src/snsschmitttrigger.cpp \
    ../../libs/signals-and-systems/src/snssum.cpp \
    ../../libs/signals-and-systems/src/snssystem.cpp


INCLUDEPATH += ../../libs/signals-and-systems/src

