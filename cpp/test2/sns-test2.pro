TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
	../../libs/snaiks/src/snsclockdevider.cpp \
	../../libs/snaiks/src/snsclockmaster.cpp \
	../../libs/snaiks/src/snsmemorymanager.cpp \
	../../libs/snaiks/src/snssystem.cpp \


INCLUDEPATH += ../../libs/snaiks/src

HEADERS += \
	../../libs/snaiks/src/snsclockdevider.h \
	../../libs/snaiks/src/snsclockmaster.h \
	../../libs/snaiks/src/snsconstant.h \
	../../libs/snaiks/src/snsconverter.h \
	../../libs/snaiks/src/snshybrid.h \
	../../libs/snaiks/src/snsinput.h \
	../../libs/snaiks/src/snslogicand.h \
	../../libs/snaiks/src/snsmemorymanager.h \
	../../libs/snaiks/src/snsnumeric.h \
	../../libs/snaiks/src/snsoutputcout.h \
	../../libs/snaiks/src/snsschmitttrigger.h \
	../../libs/snaiks/src/snssum.h \
	../../libs/snaiks/src/snssystem.h \
    ../../libs/snaiks/src/snsdelay.h

